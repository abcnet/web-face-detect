# import sys
# # from scipy import misc
# import scipy
# from scipy import misc
# import numpy as np
# def proc1(image):
# 	def weightedAverage(pixel):
# 		return 0.299*pixel[0] + 0.587*pixel[1] + 0.114*pixel[2]

# 	grey = np.zeros((image.shape[0], image.shape[1])) # init 2D numpy array
# # get row number
# 	for rownum in range(len(image)):
# 	   for colnum in range(len(image[rownum])):
# 	      grey[rownum][colnum] = weightedAverage(image[rownum][colnum])
# 	return grey

# # print 'Number of arguments:', len(sys.argv), 'arguments.'
# # print 'Argument List:', str(sys.argv)
# # n=len(sys.argv)-1
# image = misc.imread(sys.argv[1])


# # f = open(sys.argv[1], 'r')
# # image=f.read()
# # f.close()
# new_image=proc1(image)
# misc.imsave(sys.argv[1]+'-proc1.jpg',new_image)


import cv2
import sys
import os

if len(sys.argv) <= 1:
    print "Usage: greyscale Path/to/Image"
    sys.exit(-1)
elif not os.path.exists(sys.argv[1]):
    print "File not found"
    sys.exit(-1)

filename = sys.argv[1]

img = cv2.imread(filename);
img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

cv2.imwrite(filename+'-gray.jpg', img)
print 'asdf'
