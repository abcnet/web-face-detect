#! /bin/bash
# Installs deep manifold traversal on a nikola machine in to deepmanifold/
git clone https://github.com/paulu/deepmanifold/
cd deepmanifold
ln -s /usr/local/caffe ../caffe
cd images/
wget http://vis-www.cs.umass.edu/lfw/lfw.tgz
tar -zxf lfw.tgz
rm lfw.tgz
pip install Django==1.9.2
