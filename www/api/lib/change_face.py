import numpy as np
import cv2
import os
import string
import random
from random import randint
import datetime
# from django.core.files import File

cwd = os.path.dirname(os.path.abspath(__file__))
STATIC_FILE_PATH = cwd + "/../static/"

def save_to_statics(img, name, use_random = True):
    """
    Save the image into the statics files directory, return the path
    """
    path = cwd
    if use_random:
        new_id = randint(0,1000000)
        date = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        full_name = "faces_tmp/"+name+"_"+date+"_"+`new_id`+".jpg"
        full_path = path + "/../static/"+full_name
    else:
        full_path = path + "/../static/"+name+".jpg"
    print ("Save to "+ full_path)
    cv2.imwrite(full_path, img)
    return full_name

def applyEffect(ori, frame, effect, dest = None):
    """
    Apply some effect in option to the image, and output the new file name.

    [img]           the image to be dealt with.
    [effect]        string of the name of the effect.
    [dest]          The destination file name.

    Return the file name of the finished product.
    """
    img = cv2.imread(STATIC_FILE_PATH + ori)
    (x,y,w,h) = frame
    # img = np.zeros(img.shape)
    if effect == "laplacian":
        newimage = cv2.Laplacian(img[y:y+h,x:x+w],cv2.CV_64F)
        # return
    elif effect == "gaussian":
        newimage = cv2.GaussianBlur(img[y:y+h,x:x+w], (5,5), 2)
    else:
        newimage = img[y:y+h,x:x+w]
        # return
    img[y:y+h,x:x+w] = newimage
    if dest == None:
        return save_to_statics(img, effect)
    else:
        return save_to_statics(img, name, use_random = False)

if __name__ == "__main__":
    print ("TESTING CHANGE FACE LIBRARY.")
    print applyEffect('test_assets/2.jpg', (0,0,200,200),"laplacian")
