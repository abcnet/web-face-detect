import numpy as np
import cv2
import os
from random import randint
import datetime

from os import listdir
from os.path import isfile, join


mypath='/Users/zr/Documents/OneDrive/!Cornell/!Courses/ECE6931/trump'


n=1

def handle_image(img, name = None, config = None):
    """
    input: given the image
    output: return a list of file names that contains the names of cropped
            face images saved into the statics folder
    """

    if name == None:
        # Reference: http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
        import string
        import random
        def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
            return ''.join(random.choice(chars) for _ in range(size))
        name = id_generator()

    cwd = os.path.dirname(os.path.abspath(__file__))
    face_cascade = cv2.CascadeClassifier(cwd + '/haarcascade_frontalface_default.xml')
    # eye_cascade = cv2.CascadeClassifier(cwd + '/haarcascade_eye.xml')

    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # cv2.imshow('gray',gray)

    faces = face_cascade.detectMultiScale(img, 1.2, 3)
    num_faces = len(faces)
    print("Detected " + `num_faces`+ " faces." )

    count = 0;
    faces_lst = []
    for (x,y,w,h) in faces:
        # car out the image
        count = count+ 1;
        new_face = img[y:y+h, x:x+w]
        # faces_lst.append(new_face)

        # roi_gray = gray[y:y+h, x:x+w]
        # roi_color = img[y:y+h, x:x+w]
        # eyes = eye_cascade.detectMultiScale(roi_gray)
        # for (ex,ey,ew,eh) in eyes:
        #     cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

    # Second path draw the rectangle
    # for (x,y,w,h) in faces:
        # cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

    # Third path: trumpify
    # if True:
        # trump = cv2.imread(cwd + '/test_assets/trump.jpg')
        # print trump.shape
        # tw, th, _ = trump.shape
        # for (x,y,w,h) in faces:
        res = cv2.resize(new_face,(128, 128), interpolation = cv2.INTER_CUBIC)
        # img[y:(y+h),x:(x+w)] = res
        print "writing to "+str(n)+'.jpg'
        cv2.imwrite(mypath+'/trump-'+str(n)+'.jpg', res)
        global n
        n += 1





if __name__ == "__main__":

	for f in listdir(mypath):
		if isfile(join(mypath, f)):
			img = cv2.imread(join(mypath, f))
			# res = cv2.resize(img,(128, 128), interpolation = cv2.INTER_CUBIC)
			
			handle_image(img, 'test')
			
