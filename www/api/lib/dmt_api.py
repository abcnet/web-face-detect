import dmt
import os
import numpy as np
from scipy import misc

cwd = os.path.dirname(os.path.abspath(__file__))
trump_dir  = cwd + "/data/trumps"
ntrump_dir = cwd + "/data/ntrumps"
data_dir = cwd + "/data/all_data"
tmp_dir = cwd + "/data/tmp"

trump_files = [(trump_dir + "/" + x) for x in os.listdir(trump_dir) if x.endswith(".jpg") or x.endswith(".png")]
ntrump_files= [(ntrump_dir + "/" + x) for x in os.listdir(ntrump_dir) if x.endswith(".jpg") or x.endswith(".png")]
data_files= [(data_dir + "/" + x) for x in os.listdir(data_dir) if x.endswith(".jpg") or x.endswith(".png")]

def trumpify(face):
    """
    Will bring the face into trump
    """
    tmpfile = tmp_dir + "/test.png"
    misc.imsave(tmpfile, face)
    ipath = ntrump_files + trump_files + data_files + [tmpfile]
    N = len(ntrump_files)
    M = len(trump_files)
    L = len(data_files)
    device_id = 0
    weights = [1.]
    rbf_varm = 10 # Need to tune this parameters
    prefix = "trump"
    print (len(ipath), N, M, L)
    XF,F2,root_dir,result = dmt.run(ipath,N,M,L,'vgg',[128,128],device_id,weights,rbf_varm,prefix,3000,False,False)
    return result[0]


if __name__ == "__main__" :
    face = misc.imread(cwd + "/test_assets/test.jpg")
    ret = trumpify(face)
    misc.imsave(cwd + "/test_assets/ret.jpg", ret)
