import numpy as np
import cv2
import os
from random import randint
import datetime
# from django.core.files import File

cwd = os.path.dirname(os.path.abspath(__file__))
FACE_SIZE = (256,256)

def save_to_statics(img, name):
    """
    Save the image into the statics files directory, return the path
    """
    path = cwd = os.path.dirname(os.path.abspath(__file__))
    new_id = randint(0,1000000)
    date = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    full_name = "faces_tmp/"+name+"_"+date+"_"+`new_id`+".jpg"
    full_path = path + "/../static/"+full_name
    print ("Save to "+ full_path)
    cv2.imwrite(full_path, img)
    return full_name

def adjustSize(x,y,w,h):
    # adjust the size of the window
    y = int(max(y-0.5*h,0))
    x = int(max(x-0.5*w,0))
    w = 2 * w
    h = 2 * h
    return x, y, w, h

def handle_image(img, name = None, config = None):
    """
    input: given the image
    output: return a list of file names that contains the names of cropped
            face images saved into the statics folder
    """

    if name == None:
        # Reference: http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
        import string
        import random
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

    face_cascade = cv2.CascadeClassifier(cwd + '/clf/haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier(cwd + '/clf/haarcascade_eye.xml')
    eye_glass_cascade = cv2.CascadeClassifier(cwd + '/clf/haarcascade_eye_tree_eyeglasses.xml')
    eye_left_cascade = cv2.CascadeClassifier(cwd + '/clf/haarcascade_lefteye_2splits.xml')
    eye_right_cascade = cv2.CascadeClassifier(cwd + '/clf/haarcascade_righteye_2splits.xml')

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.2, 3)
    num_faces = len(faces)
    print("Detected " + `num_faces`+ " faces." )

    faces_lst = []
    eyes_lst = []
    gaussian_lst = []
    laplacian_lst = []
    rm_idx = []
    for count, (x,y,w,h) in enumerate(faces):
        x,y,w,h = adjustSize(x, y, w, h)
        roi_gray = gray[y:y+h, x:x+w]
        face = img[y:y+h, x:x+w]
        roi_color = gray[y:y+h, x:x+w]

        if roi_color.shape[0] * roi_color.shape[1] < 256*256:
            print("Size too small:{}".format(roi_color.shape))
            roi_color = cv2.resize(roi_color, (256,256), interpolation = cv2.INTER_CUBIC)
            roi_gray = cv2.resize(roi_gray, (256,256), interpolation = cv2.INTER_CUBIC)

        eyes = eye_cascade.detectMultiScale(roi_gray, scaleFactor=1.2)
        eyes_glasses = eye_glass_cascade.detectMultiScale(roi_gray, scaleFactor=1.2)
        left_eyes = eye_left_cascade.detectMultiScale(roi_gray, scaleFactor=1.2)
        right_eyes= eye_right_cascade.detectMultiScale(roi_gray, scaleFactor=1.2)

        eyes_lst.append({
            "raw_eyes"      : [frame.tolist() for frame in eyes],
            "eyes_glasses"  : [frame.tolist() for frame in eyes_glasses],
            "left_eyes"     : [frame.tolist() for frame in left_eyes],
            "right_eyes"    : [frame.tolist() for frame in right_eyes]
        })

        print ("Detect {} eyes".format(len(eyes)))
        print ("Detect {} left eyes".format(len(left_eyes)))
        print ("Detect {} right eyes".format(len(right_eyes)))
        print ("Detect {} eyes and galsses".format(len(eyes)))

        if len(eyes_glasses) + len(eyes) + len(left_eyes) + len(right_eyes) == 0:
            rm_idx.append(count)
            continue

        def downScale(w,h,original):
            if w * h > original[0] * original[1] :
                return int(w/256.0*original[0]), int(h/256.0*original[1])
            return w,h

        # for (ex,ey,ew,eh) in eyes:
        #     ew, eh = downScale(ew, eh, (256,256))
        #     cv2.rectangle(face,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
        #     # cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

        # for (ex,ey,ew,eh) in eyes_glasses:
        #     ew, eh = downScale(ew, eh, (256,256))
        #     cv2.rectangle(face,(ex,ey),(ex+ew,ey+eh),(0,0,255),2)
        #     # cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,0,255),2)

        # for (ex,ey,ew,eh) in left_eyes:
        #     ew, eh = downScale(ew, eh, (256,256))
        #     cv2.rectangle(face,(ex,ey),(ex+ew,ey+eh),(0,255,255),2)
        #     # cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,255),2)

        # for (ex,ey,ew,eh) in eyes_glasses:
        #     ew, eh = downScale(ew, eh, (256,256))
        #     cv2.rectangle(face,(ex,ey),(ex+ew,ey+eh),(255,0,255),2)
        #     # cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(255,0,255),2)

        face = cv2.resize(face, FACE_SIZE, interpolation = cv2.INTER_CUBIC)
        original_face = save_to_statics(face, (name+"_original_"+`count`))
        g_face = save_to_statics(gaussian(face), (name+"_gaussian_"+`count`))
        l_face = save_to_statics(laplacian(face), (name+"_laplacian_"+`count`))
        faces_lst.append(original_face)
        gaussian_lst.append(g_face)
        laplacian_lst.append(l_face)

    # Filter out none faces
    faces    = [ list(adjustSize(f[0],f[1],f[2],f[3])) for idx, f in enumerate(faces)  if idx not in rm_idx]
    eyes_lst = [eye_info for idx, eye_info in enumerate(eyes_lst) if idx not in rm_idx]

    # Second path draw the rectangle
    for (x,y,w,h) in faces:
        # adjust the size of the window
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

    full_img_name = save_to_statics(img, name+"_full")

    combine_lst = []
    for i in range(0, len(faces_lst)):
        face_ret = {
            "original"  :   faces_lst[i],
            "gaussian"  :   gaussian_lst[i],
            "laplacian" :   laplacian_lst[i],
            "frame"     :   faces[i],
            "eyes"      :   eyes_lst[i]
        }
        combine_lst.append(face_ret)
    ret =  {"full":full_img_name, "faces":combine_lst }

    return ret

def laplacian(img):
    return cv2.Laplacian(img,cv2.CV_64F)

def gaussian(img):
    return cv2.GaussianBlur(img, (5,5), 0.5)

if __name__ == "__main__":
    cwd = os.path.dirname(os.path.abspath(__file__))
    print(cwd)
    img = cv2.imread( cwd + '/test_assets/team_1.jpg')
    handle_image(img, 'test')
    cv2.imshow('img',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
