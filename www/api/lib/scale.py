import numpy as np
import cv2
import os
from random import randint
import datetime

from os import listdir
from os.path import isfile, join



mypath = '/Users/zr/Documents/OneDrive/!Cornell/!Courses/ECE6931/face_png'
# onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

def save_to_statics(img, name):
    """
    Save the image into the statics files directory, return the path
    """
    path = cwd = os.path.dirname(os.path.abspath(__file__))
    new_id = randint(0,1000000)
    date = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    full_name = "faces_tmp/"+name+"_"+date+"_"+`new_id`+".jpg"
    full_path = path + "/../static/"+full_name
    print ("Save to "+ full_path)
    cv2.imwrite(full_path, img)
    return full_name

for f in listdir(mypath):
	if isfile(join(mypath, f)):
		img = cv2.imread(join(mypath, f))
		res = cv2.resize(img,(128, 128), interpolation = cv2.INTER_CUBIC)
		cv2.imwrite(join(mypath, f)+'-scaled.png', res)