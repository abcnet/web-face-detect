from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='api_empty'),
    url(r'^face-detect$', views.face_detect, name='api_face_detect'),
    url(r'^apply-effect$', views.apply_effect, name='api_face_detect')
]
