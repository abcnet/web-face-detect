from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import numpy as np
import urllib
import json
import cv2

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the api URL!")

@csrf_exempt
def face_detect(request):
    """
    References: http://www.pyimagesearch.com/2015/05/11/creating-a-face-detection-api-with-python-and-opencv-in-just-5-minutes/
    """
    # initialize the data dictionary to be returned by the request
    data = {"success": False}

    # check to see if this is a post request
    if request.method == "POST":
        # check to see if an image was uploaded
        if request.FILES.get("image", None) is not None:
            # grab the uploaded image
            image = _grab_image(stream=request.FILES["image"])

        # otherwise, assume that a URL was passed in
        else:
            # grab the URL from the request
            url = request.POST.get("url", None)

            # if the URL is None, then return an error
            if url is None:
                data["error"] = "No URL provided."
                return JsonResponse(data)

            # load the image and convert
            image = _grab_image(url=url)

        if image is not None:
            from .lib import face_detect
            ret = face_detect.handle_image(image)

            data['ret'] = ret

            # update the data dictionary
            data['error'] = ''
            data["success"] = True
        else:
            data['error'] = "Failed to grab image"

    # return a JSON response
    return JsonResponse(data)

@csrf_exempt
def apply_effect(request):
    """
    References: http://www.pyimagesearch.com/2015/05/11/creating-a-face-detection-api-with-python-and-opencv-in-just-5-minutes/
    """
    # initialize the data dictionary to be returned by the request
    data = dict()

    # check to see if this is a post request
    if request.method == "POST":

        ori = request.POST.get("ori", None)
        effects = request.POST.get("effects", None)
        print(effects, ori,request.POST)
        effects_decoded = json.loads(effects)
        from .lib import change_face
        img = ori
        for i in range(len(effects_decoded)):
            x = effects_decoded[i][u'x']
            y = effects_decoded[i][u'y']
            w = effects_decoded[i][u'w']
            h = effects_decoded[i][u'h']
            effect = effects_decoded[i][u'effect']
            img = change_face.applyEffect(img, (x,y,w,h), effect)

        data['presend_addr'] = img

    # return a JSON response
    return JsonResponse(data)

def _grab_image(path=None, stream=None, url=None):
    """
    References: http://www.pyimagesearch.com/2015/05/11/creating-a-face-detection-api-with-python-and-opencv-in-just-5-minutes/
    """
    # if the path is not None, then load the image from disk
    if path is not None:
        image = cv2.imread(path)

    # otherwise, the image does not reside on disk
    else:
        # if the URL is not None, then download the image
        if url is not None:
            resp = urllib.urlopen(url)
            data = resp.read()

        # if the stream is not None, then the image has been uploaded
        elif stream is not None:
            data = stream.read()

        # convert the image to a NumPy array and then read it into
        # OpenCV format
        image = np.asarray(bytearray(data), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image
