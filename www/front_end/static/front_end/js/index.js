// Currently Using JQuery:


// Angular JS for Future
var app = angular.module('faceDetect', []).config(function($interpolateProvider) {
    // This configuration is required to distinguish from djingo template tag
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
});;

app.controller('faceDetectCtrl', ["$http","$scope",function($http, $scope){

  $scope.ori_file_name = "";
  $scope.main_file_name = "";
  $scope.face_lst = [];
  $scope.op_window_id = -1;
  $scope.effect_lst = [];

  $scope.applyEffects = function(){
    var effects = $scope.effect_lst.map(function(elet){
      return {
        "effect" : elet["effect"],
        "x"      : elet["x"],
        "y"      : elet["y"],
        "w"      : elet["w"],
        "h"      : elet["h"]
      }
    });
    $http({
        method: 'POST',
        url: '/api/apply-effect',
        data: $.param({
          "ori":$scope.ori_file_name,
          "effects": JSON.stringify(effects),
        }),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function(data){
      if (data.statusText == "OK"){
        console.log(data)
        $scope.main_file_name = "/static/"+data.data.presend_addr;
        $scope.face_lst.forEach(function(elt, idx){
          $scope.face_lst[idx]["currentEffect"] = $scope.face_lst[idx]["effectToBeApplied"]
        });
        $scope.effect_lst = [];
      }else{
        // do something
        console.log(data)
        alert("Failure")
      }
    }, function(error){
      console.log(error)
      alert("Failure")
    });
  };

  $scope.addEffect = function(idx, effect){
    $scope.face_lst[idx].effectToBeApplied = effect;

    $scope.effect_lst = $scope.effect_lst.filter(function(elt){
      return elt['idx'] != idx;
    });

    if ($scope.face_lst[idx].currentEffect != effect){
      $scope.effect_lst.push({
        "idx"    : idx,
        "target" : $scope.face_lst[idx].img,
        "effect" : effect,
        "x"      : $scope.face_lst[idx].frame[0],
        "y"      : $scope.face_lst[idx].frame[1],
        "w"      : $scope.face_lst[idx].frame[2],
        "h"      : $scope.face_lst[idx].frame[3]
      });
    }
  };

  $scope.removeEffect = function(idx) {
    $scope.effect_lst = $scope.effect_lst.filter(function(elt){
      return elt['idx'] != idx;
    })
    $scope.face_lst[idx].effectToBeApplied = "original"
  };

  // Reference: http://stackoverflow.com/questions/17922557/angularjs-how-to-check-for-changes-in-file-input-fields
  $scope.fileNameChanged = function(){
    console.log('File selected')
    console.log(angular.element(document).find('#file_input').html())
    file_lst = angular.element(document).find('#file_input')[0].files;

    var fd = new FormData();
    fd.append("image", file_lst[0]);

    $.ajax({
      "async": true,
      "crossDomain": true,
      "url": "/api/face-detect",
      "method": "POST",
      "headers": {
        "cache-control": "no-cache",
        "postman-token": "b80e42de-6601-24f2-3838-51928c003e4b"
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": fd
    }).done(function (response) {
      // inside the jQuery Method, need to call the $scope.$apply()
      console.log(response);
      data_obj = eval("("+response+")") // TODO: LOW SECURITY
      $scope.ori_file_name = data_obj.ret.full;
      $scope.main_file_name = "/static/"+data_obj.ret.full;
      $scope.face_lst = data_obj.ret.faces.map(function(elt){
        return {"img":'/static/' + elt.original,
                "frame": elt.frame,
                "effectToBeApplied" : "original",
                "currentEffect" : "original"
               }
      });
      $scope.$apply();
    });
  }
}]);
